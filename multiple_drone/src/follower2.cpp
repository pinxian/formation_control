#include <drone_def.h>
#include "interaction.h"
#include <mavconn/interface.h>
#include <mavconn/serial.h>
#include <mavconn/udp.h>
#include <mavconn/tcp.h>

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "follower2");
  ros::NodeHandle nh;

  //std::cout<<"prepare "<<std::endl;

  std_msgs::Float64 RATE;
  std::string RGB_ID;
  std::string rigibody("/vrpn_client_node/");
  //std::string this_ns;

  std_msgs::Float64 L_X;
  std_msgs::Float64 L_Y;
  std_msgs::Float64 L_Z;

  nh.getParam("update_rate",RATE.data);
  nh.getParam("rigidbody_id",RGB_ID);
  nh.getParam("L_X",L_X.data);
  nh.getParam("L_Y",L_Y.data);
  nh.getParam("L_Z",L_Z.data);
  drone_def drone(RGB_ID,RATE.data,L_X.data,L_Y.data,L_Z.data);
  drone.controlloop();


  return 0;
}
