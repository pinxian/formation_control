#include "drone_def.h"

using namespace std;
std::vector<std::string> rigibody_list;
#define KPx 1
#define KPy 1
#define KPz 1
typedef struct vir
{
    float yaw;
    float x;
    float y;
    float z;
};

vir vir1;
std_msgs::Float64 RATE;
std::string RGB_ID;
std::string rigibody("/vrpn_client_node/");
std::string this_ns;
std_msgs::Float64 L_X;
std_msgs::Float64 L_Y;
std_msgs::Float64 L_Z;
mavros_msgs::SetMode offb_set_mode;
mavros_msgs::CommandBool arm_cmd;
geometry_msgs::TwistStamped vs;
drone_msg::drone_target vir_msg;
mavros_msgs::PositionTarget spt_pose;

mavros_msgs::State current_state;
void state_cb(const mavros_msgs::State::ConstPtr& msg) {
    current_state = *msg;
}

//
geometry_msgs::PoseStamped host_mocap;
void host_pos(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    host_mocap = *msg;
}

ros::master::V_TopicInfo topic_list;

bool getRosTopics(ros::master::V_TopicInfo& topics){
    XmlRpc::XmlRpcValue args, result, payload;
    args[0] = ros::this_node::getName();
    std::string str;
    if (!ros::master::execute("getTopicTypes", args, result, payload, true)){
        std::cout << "Failed!" << std::endl;
        return false;
    }

    topics.clear();
    for (int i = 0; i < payload.size(); ++i)
        topics.push_back(ros::master::TopicInfo(std::string(payload[i][0]), std::string(payload[i][1])));
    return true;
}



void vl_pos(const drone_msg::drone_target::ConstPtr& msg)
{
    vir1.x=msg->x;
    vir1.y=msg->y;
    vir1.z=msg->z;
    vir1.yaw=msg->yaw;
}

void follow(vir& vir, geometry_msgs::PoseStamped& host_mocap, geometry_msgs::TwistStamped* vs, float dis_x, float dis_y)
{
float errx, erry, errz, err_roll;
float ux, uy, uz;
//float dis_x = 0, dis_y = -0.5;
float local_x, local_y;

local_x = cos(vir.yaw)*dis_x+sin(vir.yaw)*dis_y;
local_y = -sin(vir.yaw)*dis_x+cos(vir.yaw)*dis_y;
//qua2eul(host_mocap);

errx = vir.x - host_mocap.pose.position.x - local_x;
erry = vir.y - host_mocap.pose.position.y - local_y;
errz = vir.z - host_mocap.pose.position.z - 0;

ux = KPx*errx;
uy = KPy*erry;
uz = KPz*errz;

vs->twist.linear.x = ux;
vs->twist.linear.y = uy;
vs->twist.linear.z = uz;

}

int main(int argc, char* argv[])// int argc, char **argv
{

    ros::init(argc, argv, "follower");

    ros::NodeHandle nh;

    nh.getParam("update_rate",RATE.data);
    nh.getParam("rigidbody_id",RGB_ID);
    nh.getParam("L_X",L_X.data);
    nh.getParam("L_Y",L_Y.data);
    nh.getParam("L_Z",L_Z.data);
    rigibody.append(RGB_ID);
    rigibody.append("/pose");

    //Publisher
    ros::Publisher local_vel_pub = nh.advertise<geometry_msgs::TwistStamped>("mavros/setpoint_velocity/cmd_vel", 10);
    ros::Publisher mocap_pos_pub = nh.advertise<geometry_msgs::PoseStamped> ("mavros/mocap/pose", 10);
    ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>("mavros/setpoint_position/local", 10);

    //Subscriber
    ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>("mavros/state", 10, state_cb);//"mavros/state"
    ros::Subscriber host_sub = nh.subscribe<geometry_msgs::PoseStamped>(rigibody, 10, host_pos);
    ros::Subscriber virtual_sub = nh.subscribe<drone_msg::drone_target>("/virtual_leader", 10, vl_pos);
    //ServiceClient
    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>("mavros/cmd/arming");
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>("mavros/set_mode");


    ros::Duration(1,0).sleep();

    getRosTopics(topic_list);

    for(ros::master::V_TopicInfo::iterator it=topic_list.begin();it!=topic_list.end();it++)
    {
       ros::master::TopicInfo &info=*it;
      if(info.name.compare(0,17,"/vrpn_client_node")==0)
      {
        rigibody_list.push_back(info.name);

      }
    }

    for(int i=0;i<rigibody_list.size();i++)
    {
      for(int j=i;j<rigibody_list.size();j++)
      {
        if(rigibody_list[i].at(rigibody_list[i].size()-6) >
           rigibody_list[j].at(rigibody_list[j].size()-6)
          )
        {
          std::swap(rigibody_list[j],rigibody_list[i]);
        }
      }
    }
    this_ns=ros::this_node::getNamespace();
    std::cout<<"this namespace       "<<this_ns<<std::endl;

    //update rate
    ros::Rate rate(RATE.data);
    ros::Time last_request=ros::Time::now();
    // Wait for FCU connection.
    while (ros::ok() && current_state.connected) {
     mocap_pos_pub.publish(host_mocap);
     ros::spinOnce();
        rate.sleep();
    }

    vs.twist.linear.x = 0;
    vs.twist.linear.y = 0;
    vs.twist.linear.z = 0;
    vs.twist.angular.x = 0;
    vs.twist.angular.y = 0;
    vs.twist.angular.z = 0;

     for(int i = 100; ros::ok() && i > 0; --i){
        mocap_pos_pub.publish(host_mocap);
        local_vel_pub.publish(vs);
        ros::spinOnce();
        rate.sleep();
        }

    offb_set_mode.request.custom_mode = "OFFBOARD";
    arm_cmd.request.value = true;
    last_request=ros::Time::now();

    while (ros::ok()) {
    mocap_pos_pub.publish(host_mocap);
        //set px4 to OFFBOARD mode

        if (current_state.mode != "OFFBOARD" &&
                (ros::Time::now() - last_request > ros::Duration(5.0))) {
            if( set_mode_client.call(offb_set_mode) &&
                    offb_set_mode.response.success) {
                ROS_INFO("Offboard enabled");
            }
            last_request = ros::Time::now();
        } else {

            if (!current_state.armed &&
                    (ros::Time::now() - last_request > ros::Duration(5.0))) {
                if( arming_client.call(arm_cmd) &&
                        arm_cmd.response.success) {
                    ROS_INFO("Vehicle armed");
                }
                last_request = ros::Time::now();
            }
        }
        /*
        ROS_INFO("update rate %f %f %f %f",vl.position.x,
                                           vl.position.y,
                                           vl.position.z,
                                           vl.yaw );
       */
        follow(vir1,host_mocap,&vs,L_X.data,L_Y.data);
        mocap_pos_pub.publish(host_mocap);
        local_vel_pub.publish(vs);
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}

