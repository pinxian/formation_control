#include <ros/ros.h>
#include <cstdio>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <drone_msg/drone_target.h>
drone_msg::drone_target vir_msg;
using namespace std;

typedef struct vir{
    float x;
    float y;
    float z;
    float roll;
};
mavros_msgs::SetMode offb_set_mode;
mavros_msgs::CommandBool arm_cmd;
mavros_msgs::PositionTarget target;
#define pi 3.14159

enum{
    reset=115,
    up=65,
    down=66,
    CW=67,
    CCW=68,
    _forward=119,
    backward=120,
    keyleft=97,
    keyright=100,
    arming=107,
    disarming=108
};
char getch()
{
    int flags = fcntl(0, F_GETFL, 0);
    fcntl(0, F_SETFL, flags | O_NONBLOCK);

    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0) {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0) {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0) {
        //perror ("read()");
    }
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror ("tcsetattr ~ICANON");
    }
    return (buf);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "leader");

    ros::NodeHandle nh;

    ros::Publisher leader_pub = nh.advertise<drone_msg::drone_target>("/virtual_leader", 1000);

    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
                                       ("drone1/mavros/cmd/arming");
    ros::ServiceClient arming_client2 = nh.serviceClient<mavros_msgs::CommandBool>
                                       ("drone2/mavros/cmd/arming");
    ros::ServiceClient arming_client3 = nh.serviceClient<mavros_msgs::CommandBool>
                                       ("drone3/mavros/cmd/arming");
    ros::ServiceClient arming_client4 = nh.serviceClient<mavros_msgs::CommandBool>
                                       ("drone4/mavros/cmd/arming");
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
                                         ("drone1/mavros/set_mode");
    ros::ServiceClient set_mode_client2 = nh.serviceClient<mavros_msgs::SetMode>
                                         ("drone2/mavros/set_mode");
    ros::ServiceClient set_mode_client3 = nh.serviceClient<mavros_msgs::SetMode>
                                         ("drone3/mavros/set_mode");
    ros::ServiceClient set_mode_client4 = nh.serviceClient<mavros_msgs::SetMode>
                                         ("drone4/mavros/set_mode");


    ros::Rate rate(30);

  vir vir1;
  vir1.x = 0;
  vir1.y = 0;
  vir1.z = 0.6;
  vir1.roll = 0;
  vir_msg.x=vir1.x;
  vir_msg.y=vir1.y;
  vir_msg.z=vir1.z;
  vir_msg.yaw=vir1.roll;
    leader_pub.publish(vir_msg);
    while (ros::ok()) {

    //input
        int c = getch();
        if (c != EOF) {
            switch (c) {
            case up:    // key up
                vir1.z += 0.05;
                break;
            case down:    // key down
                vir1.z += -0.05;
                break;
            case CW:    // key CW
                vir1.roll -= 0.03;
                break;
            case CCW:    // key CCW
                vir1.roll += 0.03;
                break;
            case _forward:    // key foward
                vir1.x += 0.05;
                break;
            case backward:    // key back
                vir1.x += -0.05;
                break;
            case keyleft:    // key left
                vir1.y += 0.05;
                break;
            case keyright:    // key right
                vir1.y -= 0.05;
                break;
            case reset:    // key right
        {
                vir1.x = 0;
                vir1.y = 0;
                vir1.z = 0;
                vir1.roll = 0;
                break;
        }
            case arming:
              offb_set_mode.request.custom_mode = "OFFBOARD";
              set_mode_client.call(offb_set_mode);
              set_mode_client2.call(offb_set_mode);
              set_mode_client3.call(offb_set_mode);
              set_mode_client4.call(offb_set_mode);
              arm_cmd.request.value = true;
              arming_client.call(arm_cmd);
              arming_client2.call(arm_cmd);
              arming_client3.call(arm_cmd);
              arming_client4.call(arm_cmd);
              ROS_INFO("offboard enabled");
              ROS_INFO("start arming");
            case disarming:
              offb_set_mode.request.custom_mode = "MANUAL";
              set_mode_client.call(offb_set_mode);
              set_mode_client2.call(offb_set_mode);
              set_mode_client3.call(offb_set_mode);
              set_mode_client4.call(offb_set_mode);
              arm_cmd.request.value = false;
              arming_client.call(arm_cmd);
              arming_client2.call(arm_cmd);
              arming_client3.call(arm_cmd);
              arming_client4.call(arm_cmd);
              ROS_INFO("offboard disabled");
              ROS_INFO("stop arming");
            case 63:
                return 0;
                break;
            }
        }
     /*
       target.position.x=vir1.x;
       target.position.y=vir1.y;
       target.position.z=vir1.z;
       target.yaw = vir1.roll;
     */
       vir_msg.x=vir1.x;
       vir_msg.y=vir1.y;
       vir_msg.z=vir1.z;
       vir_msg.yaw=vir1.roll;
       ROS_INFO("setpoint: %.2f, %.2f, %.2f, %.2f", vir1.x, vir1.y, vir1.z,vir1.roll/pi*180);
       leader_pub.publish(vir_msg);
       ros::spinOnce();
       rate.sleep();
    }

    return 0;
}

