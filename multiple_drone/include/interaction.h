#ifndef INTERACTION_H
#define INTERACTION_H

#include"ros/ros.h"
class interaction
{
public:
  interaction(std::string host_name);
  //ros::master::V_TopicInfo drone_list;
  ros::master::V_TopicInfo topic_list;
  std::vector<std::string> drone_list;
  std::vector<std::string> neighbor_list;
  void get_neighbor_list(std::string host_name);

private:
  bool getRosTopics(ros::master::V_TopicInfo& topics)  ;




};

#endif // INTERACTION_H
