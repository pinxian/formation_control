#ifndef DRONE_DEF_H
#define DRONE_DEF_H

//include basic io header file
#include <ros/ros.h>
#include <cstdio>
#include <sstream>
#include "math.h"
//std_msgs typedef
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>

//include mavros header file
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>

//include ros transform header file
#include <tf/tfMessage.h>
#include <tf/transform_datatypes.h>
#include <tf/tf.h>

//include ros geometry_msgs header file
//#include <geometry_msgs/TwistStamped.h>
//#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>

//include drone definition header file
#include "drone_msg/drone_target.h"


/*
 *PID
*/
#define KP_X 1
#define KP_Y 1
#define KP_Z 1

#define KI_X 0.00
#define KI_Y 0.00
#define KI_Z 0.00

#define KI_X_LIMIT 0.3
#define KI_Y_LIMIT 0.3
#define KI_Z_LIMIT 0.3

#define KD_X 0.0
#define KD_Y 0.0
#define KD_Z 0.0
/*

*/

class drone_def
{
public:
  drone_def(std::string name,float rate,float x,float y ,float z);



//  std_msgs::Float64 RATE;     //roslaunch param
//  std::string RGB_ID;
//  std::string this_ns;

  std::string rigibody;  //("/vrpn_client_node/")
  float L_X;
  float L_Y;
  float L_Z;

  void controlloop();


private:
  //ROS msg & srv
  mavros_msgs::SetMode offb_set_mode; //mavros
  mavros_msgs::CommandBool arm_cmd;
  mavros_msgs::PositionTarget spt_pose;
  mavros_msgs::State current_state;
  geometry_msgs::PoseStamped host_mocap; //mocap data
  geometry_msgs::Twist vs;  //virtual leader
  drone_msg::drone_target vir1;
  //ROS class
  ros::Time last_request;
  ros::master::V_TopicInfo topic_list;
  ros::NodeHandle nh;
  ros::Publisher local_vel_pub;
  ros::Publisher mocap_pos_pub;
  ros::Publisher local_pos_pub;
  ros::Subscriber state_sub;
  ros::Subscriber host_sub;
  ros::Subscriber virtual_sub;
  ros::ServiceClient arming_client;
  ros::ServiceClient set_mode_client;
  ros::Rate loop_rate;
  //bool getRosTopics(ros::master::V_TopicInfo& topics);

  //PID
  float integral_x;
  float integral_y;
  float integral_z;
  float last_errx;
  float last_erry;
  float last_errz;
  //drone
  void follow(drone_msg::drone_target& vir, geometry_msgs::PoseStamped& host_mocap, geometry_msgs::Twist& vs, float dis_x, float dis_y,float dis_z);
  void set_offb_mode();
  void prepare();

  //ROS subscribe callback function
  void state_cb(const mavros_msgs::State::ConstPtr& msg);
  void host_pose_cb(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void vl_pos_cb(const drone_msg::drone_target::ConstPtr& msg);

};

#endif // DRONE_DEF_H
