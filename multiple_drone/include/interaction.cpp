#include "interaction.h"

interaction::interaction(std::string host_name)
{

  this->drone_list.clear();
  this->neighbor_list.clear();
  this->getRosTopics(topic_list);
  for(ros::master::V_TopicInfo::iterator it=topic_list.begin();it!=topic_list.end();it++)
  {
    ros::master::TopicInfo &info=*it;
    if(info.name.compare(0,6,"/drone")==0)
    {
        drone_list.push_back(info.name);
    }
  }
    std::cout<<"drone_list"<<std::endl;
      for(std::vector<std::string>::iterator it= drone_list.begin();it!=drone_list.end();it++)
      {
        std::cout<<*(it)<<std::endl;
      }
      std::cout<<"drone_list end"<<std::endl;

//      this->get_neighbor_list(host_name);
//      std::cout<<"neighbor list"<<std::endl;
//      for(std::vector<std::string>::iterator it= neighbor_list.begin();it!=neighbor_list.end();it++)
//      {
//        std::cout<<*(it)<<std::endl;
//      }
}


void interaction::get_neighbor_list(std::string host_name)
{
  std::cout<<"test"<<std::endl;
  for(std::vector<std::string>::iterator it= drone_list.begin();it!=drone_list.end();it++)
  {
   // if(host_name.compare(0,6,*it)==0)
    std::string buf=*it;
      if(buf.compare(host_name))
      {
        ;
         std::cout<<"ok"<<std::endl;
      }else{
          neighbor_list.push_back(buf);
          std::cout<<buf<<std::endl;
      }
  }
  std::cout<<"test end"<<std::endl;
}


/*
 * get all rostopics
*/
bool interaction::getRosTopics(ros::master::V_TopicInfo& topics){
    XmlRpc::XmlRpcValue args, result, payload;
    args[0] = ros::this_node::getName();
    std::string str;
    if (!ros::master::execute("getTopicTypes", args, result, payload, true)){
        std::cout << "Failed!" << std::endl;
        return false;
    }

    topics.clear();
    for (int i = 0; i < payload.size(); ++i)
        topics.push_back(ros::master::TopicInfo(std::string(payload[i][0]), std::string(payload[i][1])));
    return true;
}
