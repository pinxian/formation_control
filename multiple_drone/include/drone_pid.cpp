#include "drone_pid.h"

drone_pid::drone_pid(float hz) :   //loop_rate
   dt(1/hz),
   integral_x(0) , integral_y(0) , integral_z(0),
   last_errx (0) , last_erry (0) , last_errz (0)
{


}
/*
 *
 * https://zh.wikipedia.org/wiki/PID%E6%8E%A7%E5%88%B6%E5%99%A8
 *
*/
void drone_pid::compute(pid_io& error,pid_io& output){

  integral_x+=error.x*dt;
  integral_y+=error.y*dt;
  integral_z+=error.z*dt;

  //set output limit to prevent drone from being out of control
  integral_x = set_outputl_imit(integral_x,KI_X_LIMIT);
  integral_y = set_outputl_imit(integral_y,KI_Y_LIMIT);
  integral_z = set_output_limit(integral_z,KI_Z_LIMIT);


  output.x = KP_X*error.x+KI_X*integral_x*dt+KD_X*(error.x-last_errx)/dt;
  output.y = KP_Y*error.y+KI_Y*integral_y*dt+KD_Y*(error.y-last_erry)/dt;
  output.z = KP_Z*error.z+KI_Z*integral_z*dt+KD_Z*(error.z-last_errz)/dt;

  last_errx = error.x;
  last_erry = error.y;
  last_errz = error.z;

}

float drone_pid::set_outputl_imit(float value,float limit)
{
  if(value>limit)
  {
    value=limit;
  }else if(value<(-1*limit))
  {
    value=-1*limit;
  }
  return value;
}


