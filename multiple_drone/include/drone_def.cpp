#include "drone_def.h"


/*
 定義一台新的無人機
 std::string name = mocap system 上對應的 rigidbody 名稱
 float rate  程式迴圈執行的速率 Hz
 float l_x   無人機相對virtual leader的x座標
 float l_y   無人機相對virtual leader的y座標
 float l_z   無人機相對virtual leader的z座標
 *
*/
drone_def::drone_def(
    std::string name,
    float rate,
    float l_x,
    float l_y,
    float l_z
    ) :
  rigibody("/vrpn_client_node/"),  //
  loop_rate(rate),
  integral_x(0),integral_y(0),integral_z(0),
  last_errx(0) , last_erry(0),last_errz(0)

{

    this->rigibody.append(name);
    this->rigibody.append("/pose");


    local_vel_pub = nh.advertise<geometry_msgs::Twist>("mavros/setpoint_velocity/cmd_vel_unstamped", 10);
    mocap_pos_pub = nh.advertise<geometry_msgs::PoseStamped> ("mavros/mocap/pose", 10);
    local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>("mavros/setpoint_position/local", 10);

    state_sub = nh.subscribe<mavros_msgs::State>("mavros/state", 10, &drone_def::state_cb ,this);
    host_sub = nh.subscribe<geometry_msgs::PoseStamped>(this->rigibody, 10, &drone_def::host_pose_cb,this);
    virtual_sub = nh.subscribe<drone_msg::drone_target>("/virtual_leader", 10, &drone_def::vl_pos_cb,this);

    arming_client = nh.serviceClient<mavros_msgs::CommandBool>("mavros/cmd/arming");
    set_mode_client = nh.serviceClient<mavros_msgs::SetMode>("mavros/set_mode");

    this->L_X=l_x;
    this->L_Y=l_y;
    this->L_Z=l_z;
}




/*
 *可以讓無人機跟隨virtual leader的控制器
 *drone_msg::drone_target& vir  目前virtual leader的位置
 *geometry_msgs::PoseStamped &host_mocap 目前無人機在mocap system 中的座標
 * geometry_msgs::Twist& v_s 控制器算出無人機目前要輸出的速度
 * float dis_x 無人機相對virtual leader的x座標
 * float dis_y 無人機相對virtual leader的y座標
 * float dis_z 無人機相對virtual leader的z座標
*/
void drone_def::follow(drone_msg::drone_target& vir, geometry_msgs::PoseStamped &host_mocap, geometry_msgs::Twist& v_s, float dis_x, float dis_y ,float dis_z)
{
  float errx, erry, errz;
  float ux, uy, uz;
  float local_x, local_y;

  local_x = cos(vir.yaw)*dis_x+sin(vir.yaw)*dis_y;
  local_y = -sin(vir.yaw)*dis_x+cos(vir.yaw)*dis_y;

  errx = vir.x - host_mocap.pose.position.x - local_x;
  erry = vir.y - host_mocap.pose.position.y - local_y;
  errz = vir.z - host_mocap.pose.position.z - dis_z;

  integral_x+=errx;
  integral_y+=erry;
  integral_z+=errz;

  if(integral_x>KI_X_LIMIT)
  {
    integral_x=KI_X_LIMIT;
  }else if(integral_x<-1*KI_X_LIMIT){
    integral_x=-1*KI_X_LIMIT;
  }

  if(integral_y>KI_Y_LIMIT)
  {
    integral_y=KI_Y_LIMIT;
  }else if(integral_y<-1*KI_Y_LIMIT){
    integral_y=-1*KI_Y_LIMIT;
  }

  if(integral_z>KI_Z_LIMIT)
  {
    integral_z=KI_Z_LIMIT;
  }else if(integral_z<-1*KI_Z_LIMIT){
    integral_z=-1*KI_Z_LIMIT;
  }
/*
  if((errx-last_errx)>KI_Z_LIMIT)
  {
    integral_z=KI_Z_LIMIT;
  }else if((errx-last_errx)<-1*KI_Z_LIMIT){
    integral_z=-1*KI_Z_LIMIT;
  }
*/

  ux = KP_X*errx+KI_X*integral_x+KD_X*(errx-last_errx);    //KP_X=1
  uy = KP_Y*erry+KI_Y*integral_y+KD_Y*(erry-last_erry);    //KP_Y=1
  uz = KP_Z*errz+KI_Z*integral_z+KD_Z*(errz-last_errz);    //KP_Z=1

  v_s.linear.x=ux;
  v_s.linear.y=uy;
  v_s.linear.z=uz;

  last_errx=errx;
  last_erry=erry;
  last_errz=errz;

}
/*
 * 設定讓無人機進入offboard模式
 * 無人機在offboard模式下 如果超過0.5秒未接收到指令會自動跳到進入offboard的前一個模式
*/
void drone_def::set_offb_mode()
{
  /*
   * ros::Time::now() replaced by ros::Time
  */
  //std::cout<<"\n  set_offb_mode  \n"<<std::endl;
  this->offb_set_mode.request.custom_mode="OFFBOARD";
  this->arm_cmd.request.value = true;
  if (this->current_state.mode != "OFFBOARD" &&
          (ros::Time::now() - this->last_request > ros::Duration(5.0))) {
      if( this->set_mode_client.call(this->offb_set_mode) &&
              this->offb_set_mode.response.mode_sent) {
          ROS_INFO("Offboard enabled");
      }
      this->last_request = ros::Time::now();
  } else {

      if (!this->current_state.armed &&
              (ros::Time::now() - this->last_request > ros::Duration(5.0))) {
          if( this->arming_client.call(arm_cmd) &&
                  this->arm_cmd.response.success) {
              ROS_INFO("Vehicle armed");
          }
          this->last_request = ros::Time::now();
      }
  }

}
/*
 *在執行飛行前需要先串流mocap system的資料到無人機上，確保無人機街收到的資訊正確。
 *
*/
void drone_def::prepare()
{
  while (ros::ok() && this->current_state.connected) {
   this->mocap_pos_pub.publish(this->host_mocap);
   ros::spinOnce();
      this->loop_rate.sleep();
  }
  this->vs.linear.x = 0;
  this->vs.linear.y = 0;
  this->vs.linear.z = 0;
  this->vs.angular.x = 0;
  this->vs.angular.y = 0;
  this->vs.angular.z = 0;

  for(int i = 100; ros::ok() && i > 0; --i)
    {
       this->mocap_pos_pub.publish(host_mocap);
       this->local_vel_pub.publish(vs);
       ros::spinOnce();
      this->loop_rate.sleep();
    }
}


/*
 *
 *飛機執行控制器的迴圈 ，在主程式中呼叫。
 *
*/
void drone_def::controlloop()
{
  this->prepare();     //streaming data to host_mocap
  while(ros::ok())
  {
    this->set_offb_mode();   //set drone offb mode
    follow(this->vir1,this->host_mocap,this->vs,this->L_X,this->L_Y,this->L_Z);

    /*
    std::cout <<"vir"<<this->vir1.x
                     <<this->vir1.y
                     <<this->vir1.z
                    <<this->vir1.yaw<<std::endl;
    */
    mocap_pos_pub.publish(host_mocap);
    local_vel_pub.publish(vs);
    ros::spinOnce();
    loop_rate.sleep();
  }
}



/*
 * callback function
*/
void drone_def::host_pose_cb(const geometry_msgs::PoseStamped::ConstPtr& msg)
{

   this->host_mocap = *msg;
}

void drone_def::state_cb(const mavros_msgs::State::ConstPtr& msg)
{

  this->current_state = *msg;
}

void drone_def::vl_pos_cb(const drone_msg::drone_target::ConstPtr& msg)
{

  this->vir1.x=msg->x;      //" this ->vir1=*msg "should be test
  this->vir1.y=msg->y;
  this->vir1.z=msg->z;
  this->vir1.yaw=msg->yaw;   //this ->vir1=*msg;
}
