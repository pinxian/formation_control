#ifndef DRONE_PID_H
#define DRONE_PID_H


//PID paramter
#define KP_X 1
#define KP_Y 1
#define KP_Z 1

#define KI_X 0.001
#define KI_Y 0.001
#define KI_Z 0.001

#define KI_X_LIMIT 0.3
#define KI_Y_LIMIT 0.3
#define KI_Z_LIMIT 0.3

#define KD_X 0.00
#define KD_Y 0.00
#define KD_Z 0.00
//#define Ts  1    //sampling

class drone_pid
{
struct d_vector   //dof
{
  float x;
  float y;
  float z;
};
typedef pid_io d_vector;

public:
  float integral_x;
  float integral_y;
  float integral_z;
  float last_errx;
  float last_erry;
  float last_errz;

//  pid_io input;
//  pid_io output;
  drone_pid(float hz);
private:
  float set_output_limit(float value,float limit);
  void compute(pid_io& in,pid_io& out);
};

#endif // DRONE_PID_H
